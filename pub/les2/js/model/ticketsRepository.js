define(['localStorage'], function (localStorage) {
    'use strict'

    const key = 'tickets'

    function generateId() {
        return Math.random().toString(36).substr(2, 9)
    }

    return {
        getTickets: function () {
            return localStorage.getDataByKey(key)
        },

        getTicket: function (id) {
            let tickets = this.getTickets();

            for (let i = 0; tickets && i < tickets.length; i++) {
                if (tickets[i].id === id) {
                    return tickets[i]
                }
            }

            return null
        },

        saveTicket: function (ticket) {
            if (!ticket.id) {
                ticket.id = generateId()
            }

            let tickets = this.getTickets();
            let updated = false;

            if (!tickets || !tickets.length) {
                tickets = [];
            }

            for (let i in tickets) {
                if (tickets[i].id === ticket.id) {
                    tickets[i] = ticket;
                    updated = true;
                    break;
                }
            }

            if (!updated) {
                tickets.push(ticket);
            }

            localStorage.setData(key, tickets)
        },

        deleteTicket: function (id) {
            let tickets = this.getTickets();

            if (!tickets || !tickets.length) {
                return;
            }

            for (let i = 0; i < tickets.length; i++) {
                if (tickets[i].id === id) {
                    tickets.splice(i, 1);
                    break;
                }
            }

            localStorage.setData(key,tickets);
        }
    }
})