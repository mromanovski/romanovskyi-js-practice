define(['localStorage'], function (localStorage) {
    'use strict'

    const key = 'current-item'

    return {
        setContext: function (id) {
            localStorage.setData(key, id)
        },

        getContext: function () {
            return localStorage.getDataByKey(key)
        }
    }
})