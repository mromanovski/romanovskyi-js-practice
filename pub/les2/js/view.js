define(['jquery', 'model/ticketsRepository', 'action/editRedirect'], function ($, ticketsRepo, editRedirect) {
    'use strict'

    return function () {
        let tickets = ticketsRepo.getTickets();

        for (let i in tickets) {
            let ticket = tickets[i];
            let wrapper = $('#' + ticket.status + '-list');
            let element = $('<li>').prop('id', ticket.id);
            let innerWrapper = $('<a>').appendTo(element)
                .addClass('hover:bg-yellow-200 hover:border-transparent hover:shadow-lg group block rounded-lg p-4 border border-gray-200');

            innerWrapper.append(
                $('<div>').append(
                    $('<dd>').addClass('group-hover:text-light-blue-200 text-sm font-medium sm:mb-4 lg:mb-0 xl:mb-4')
                        .append($('<span>').addClass('font-bold').text(ticket.title))
                        .append($('<br>'))
                        .append($('<span>').addClass('font-medium').text(ticket.description))
                        .append($('<br>'))
                        .append($('<br>'))
                        .append($('<span>').addClass('font-normal').text(ticket.assigned))
                )
            ).click(editRedirect(ticket))

            wrapper.append(element)
        }

        $('#new-ticket').click(editRedirect(null));
    }
})