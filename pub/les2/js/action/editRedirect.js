define(['model/context'], function (context) {
    'use strict'

    return function (ticket) {
        return function () {
            let url = location.href.split('/').slice(0,-1).join('/') + '/edit.html';
            context.setContext(ticket.id);
            location.href = url;
        }
    }
})