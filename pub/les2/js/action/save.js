define(['model/ticketsRepository'], function (repo) {
    'use strict'

    return function (id) {
        return function (ticket) {
            let url = location.href.split('/').slice(0,-1).join('/') + '/index.html';
            ticket.id = id;
            repo.saveTicket(ticket);

            location.href = url;
        }
    }
})