define(['model/ticketsRepository'], function (repo) {
    'use strict'

    return function (id) {
        return function () {
            let url = location.href.split('/').slice(0,-1).join('/') + '/index.html';
            repo.deleteTicket(id);

            location.href = url;
        }
    }
})