define([], function (){
    'use strict'

    return {
        setData: function (key, value) {
            localStorage.setItem(key, JSON.stringify(value))
        },
        getDataByKey: function (key) {
            return JSON.parse(localStorage.getItem(key))
        }
    }
})