define([
], function () {
    'use strict'

    return function (url) {
        location.href = location.href.split('/').slice(0, -1).join('/') + '/' + url + '.html';
    }
})