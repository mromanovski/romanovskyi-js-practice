define([
    'ko',
    'model/ticketsRepository',
    'action/redirect',
    'model/context'
], function (ko, ticketsRepository, redirect, context) {
    'use strict'

    return function indexViewModel() {
        let self = this,
            tasks = ticketsRepository.getTickets();

        self.tasks = ko.observableArray(tasks);

        self.isDeleteAllVisible = ko.computed(function () {
            return self.tasks().length > 5;
        });

        self.deleteAll = function () {
            let tasks = ticketsRepository.getTickets();
            for (let i =0; i < tasks.length; i++) {
                ticketsRepository.deleteTicket(tasks[i].id);
            }

            self.reload();
        };

        self.taskRedirect = function (data, event) {
            const id = data.id;
            context.setContext(id);
            redirect('edit');
        };

        self.deleteTask = function (data, event) {
            const id = data.id;
            ticketsRepository.deleteTicket(id);
            self.reload();

            return true;
        };

        self.newTaskRedirect = function () {
            context.setContext(null);
            redirect('edit');
        };

        self.reload = function () {
            self.tasks(ticketsRepository.getTickets());
        }
    }
})