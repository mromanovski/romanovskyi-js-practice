define([
    'ko',
    'jquery',
    'model/ticketsRepository',
    'action/redirect',
    'model/context'
], function (ko, $, ticketsRepository, redirect, context) {
    'use strict';

    function getDescription(text) {
        return '<p><b>' + text + '</b></p>';
    }

    return function editViewModel() {
        let self = this,
            task = ticketsRepository.getTicket(context.getContext()),
            title = '',
            description = '',
            assigned = '',
            status = '';

        if (task) {
            title = task.title;
            description = task.description;
            assigned = task.assigned;
            status = task.status;
        }

        self.availableAssigned = ['Oleks', 'Nick', 'Teo'];
        self.availableStatus = ['backlog', 'todo', 'inprogress', 'done'];

        self.title = ko.observable(title);
        self.description = ko.observable(description);
        self.selectedAssigned = ko.observable(assigned);
        self.selectedStatus = ko.observable(status);

        self.fullDescription = ko.computed(function () {
            if (self.title().length) {
                return getDescription(self.title()) +
                    getDescription(self.description()) +
                    getDescription(self.selectedAssigned()) +
                    getDescription(self.selectedStatus())
            }
        }, self);

        self.isCopyVisible = ko.computed(function () {
            let task = ticketsRepository.getTicket(context.getContext());
            return !!task;
        }, self);

        self.submitForm = function () {
            let task = ticketsRepository.getTicket(context.getContext());
            if (task) {
                task.title = self.title();
                task.description = self.description();
                task.assigned = self.selectedAssigned();
                task.status = self.selectedStatus();
            } else {
                task = {
                    title: self.title(),
                    description: self.description(),
                    assigned: self.selectedAssigned(),
                    status: self.selectedStatus()
                };
            }
            ticketsRepository.saveTicket(task);
            redirect('index');
        };

        self.goBack = function () {
            redirect('index');
        };

        self.copyTicket = function () {
            let task = ticketsRepository.getTicket(context.getContext());
            if (task) {
                task.id = null;
                ticketsRepository.saveTicket(task);
            }
            redirect('index');
        };

        self.deleteTicket = function () {
            let task = ticketsRepository.getTicket(context.getContext());
            if (task) {
                ticketsRepository.deleteTicket(task.id);
            }
            redirect('index');
        };
    }
})