define(['jquery', 'localStorage'], function ($, localStorage) {
    'use strict'

    return function () {
        let localStorageData = localStorage.getDataByKey('profile');
        if (localStorageData) {
            $('#fullname').text(localStorageData.name + ' ' + localStorageData.surname)
            $('#position').text(localStorageData.position)
            $('#edit-link').text('Edit Profile')
        } else {
            $('#fullname').text('No data')
        }
    }
})