define(['jquery','localStorage', 'validate'], function ($, localStorage) {
    'use strict'

    return function () {
        setValuesFromLocalStorage();
        clearLocalStorage();
        saveDataToTheLocalStorage();

        function setValuesFromLocalStorage() {
            let localStorageData = localStorage.getDataByKey('profile');
            if (localStorageData) {
                $('#fname').val(localStorageData.name)
                $('#lname').val(localStorageData.surname)
                $('#position').val(localStorageData.position)
            }
        }

        function clearLocalStorage() {
            $('#clear').on('click', function (){
                localStorage.clear()
                window.location.replace('index.html')
            })
        }

        function saveDataToTheLocalStorage() {
            $(document).on('click', '#save', function () {
                let valid = true;
                let validateForm = $(document).find('#form');
                validateForm.validate({
                    rules: {
                        fname: {
                            required: true
                        },
                        lname: {
                            required: true
                        }
                    },
                    messages: {
                        fname: {
                            required: "Name suka"
                        },
                        lname: {
                            required: "Last name suka"
                        }
                    },
                    submitHandler: function (form) {
                        form.submit();
                    },
                    errorPlacement: function (error, element) {
                        valid = false;
                        let item = element.parents('.item');
                        item.append(error);
                    }
                })
                validateForm.submit();
                if (valid === true) {
                    let name = $('#fname').val()
                    let surname = $('#lname').val()
                    let position = $('#position').val()
                    localStorage.setData('profile', {name: name, surname: surname, position: position})
                    window.location.replace('index.html')
                }
            });
        }
    }
})